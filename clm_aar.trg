CREATE OR REPLACE TRIGGER PC_DBA.clm_aar
  AFTER INSERT OR UPDATE OR DELETE ON pc_dba.claim
  REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW

  ------------------------------------------------------------------------------------------------
  -- Purpose      : TDX14062-Trigger to capture DML activity
  -- Created By   : VSAWARDEKAR
  -- Date Created : 12/3/2015
  --
  -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  -- BUSINESS AREA: Claim
  -- SPECIAL TAG: NONE
  -- KEYWORDS:
  -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  --
  ----------------------------------------------------------------------------
  -- Date     Modified By  Description
  ----------------------------------------------------------------------------
  --4/12/2021 jamartin     Comment for testing CD.
  ------------------------------------------------------------------------------------------------

DECLARE
  --
  --
BEGIN

  -- If this transaction is being executed by Records Rention (RIM) package (pkg_rim) then
  -- disable the trigger by not executing the rest of the code.
  --
  IF pkg_rim.f_rim_updt_mode_chck = TRUE THEN
    RETURN;
  END IF;
  --
  --
  -- [TDX14062] - Capture FROI event
  IF inserting AND :new.plcy_no IS NOT NULL THEN
    pkg_alert.prc_evnt_crea(av_agre_typ => 'clm',
                            an_agre_id => :new.Injr_Id,
                            ad_agre_prd_eff_dt => NULL,
                            av_alrt_evnt_typ_cd => 'FROI');
  ELSIF updating AND :new.plcy_no IS NOT NULL AND nvl(:new.plcy_no, 'x') <> nvl(:old.plcy_no, 'x') THEN
    pkg_alert.prc_evnt_crea(av_agre_typ => 'clm',
                            an_agre_id => :new.Injr_Id,
                            ad_agre_prd_eff_dt => NULL,
                            av_alrt_evnt_typ_cd => 'FROI');
  END IF;
  --
END;
/
