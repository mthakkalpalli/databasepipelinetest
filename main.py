import sys 
import os
from configparser import ConfigParser
from subprocess import Popen, PIPE

#Read config.ini file
config_object = ConfigParser()
config_object.read("config.ini")

scriptfile = config_object["SCRIPTFILES"]
envvariables = config_object["ENVVARIABLES"]


# We have assigned global  variables for filepath and Taskfolder

MIG = envvariables["env"]
JIRATASKID = envvariables["taskid"]

ENV=envvariables["envpath"]
TASK=envvariables["taskidpath"]

#Define a  new connnection string which should have the desire DB connection details
connectString = envvariables["dbusername"] + "/" + envvariables["dbpassword"] + "@" + envvariables["database"]
print(connectString)
 
if not os.path.exists(MIG+'/projects/stage/'+ JIRATASKID  + '/log'):
    os.makedirs(MIG+'/projects/stage/'+ JIRATASKID  + '/log')

#function that takes the sqlCommand and connectString and returns the queryReslut and errorMessage (if any)
def runSqlQuery(sqlCommand, connectString):
   session = Popen(['sqlplus', '-S', connectString], stdin=PIPE, stdout=PIPE, stderr=PIPE)
   session.stdin.write(sqlCommand)
   return session.communicate()
   
#function to replace path names 
def replacePath(inputString, replacementString, inputFile):
    #input fil
	outfile = "out"+inputFile
	fin = open(inputFile, "rt")
	#output file to write the result to
	fout = open(outfile, "wt")
	#for each line in the input file
	for line in fin:
		fout.write(line.replace(inputString, replacementString ))
	fin.close()
	fout.close()
	return outfile

# Get PKG file name from metadata file

# Using readlines()
file1 = open(scriptfile["metadata"], 'r')
Lines = file1.readlines()

# prescript execution 
prescript = replacePath(ENV, MIG, scriptfile["prescript"])
prescript = replacePath(TASK, JIRATASKID, prescript)
prescript = "@" + prescript + "\n"
with open("temp.txt", "w") as fh:
    fh.write(prescript)
# Strips the newline character
for line in Lines:
    file_name = line.strip()
    outfile = replacePath(ENV, MIG, file_name)
    outfile = replacePath(TASK, JIRATASKID, outfile)
    cmd = "@"
    sqlCommand = cmd+str(outfile) + "\n"
    with open("temp.txt", "a") as fh:
        fh.write(sqlCommand)
# postscript execution
postscript = replacePath(ENV, MIG, scriptfile["postscript"])
postscript = replacePath(TASK, JIRATASKID, postscript)
postscript = "@" + postscript + "\n"
with open("temp.txt", "a") as fh:
    fh.write(postscript)
queryResult, errorMessage = runSqlQuery("@temp.txt".encode('utf-8'), connectString)
print(queryResult)
print(errorMessage)