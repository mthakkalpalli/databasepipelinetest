CREATE OR REPLACE PACKAGE PC_DBA.pkg_clm AS
--Generic Claims Controller used by bnft_calc controller to fetch data for blocks
--defined in rating_block_expression table

FUNCTION f_lob_cd_get(an_injr_id NUMBER) RETURN VARCHAR2;
-- This function returns the line of business for an injr_id passed in.
FUNCTION f_bnft_stt_cd_get(an_injr_id NUMBER) RETURN VARCHAR2;
-- This function returns the benefit stt_cd for an injr_id passed in.
FUNCTION f_mrkt_typ_cd_get(an_injr_id NUMBER) RETURN VARCHAR2;
-- This function returns the market_type cd for an injr_id passed in.
FUNCTION f_cmpy_cd_get(an_injr_id NUMBER) RETURN VARCHAR2;
-- This function returns the company cd for an injr_id passed in.
FUNCTION f_injr_dt_get (an_injr_id NUMBER) RETURN DATE;
-- This function returns the injury_date for an injr_id passed in.
FUNCTION f_loe_get (an_injr_id NUMBER) RETURN DATE;
-- This function returns the loss of earnings date for an injr_id passed in.
FUNCTION f_aww_get (an_injr_id NUMBER) RETURN NUMBER;
-- This function returns the current Average Weekly Wage for an injr_id passed in.
FUNCTION f_post_aww_get (an_injr_id NUMBER, ad_admis_eff_dt DATE, ad_admis_end_dt DATE) RETURN NUMBER;
-- This function returns the current Post AWW for the injr_id and admission period dates passed in.
FUNCTION f_cmpn_law_cnst_hist_val_get (av_cnst_typ_cd VARCHAR2, ad_injr_dt DATE, av_bnft_typ_cd VARCHAR2) RETURN NUMBER;
-- This function returns the law constant values for a bnft_typ_cd, inkr_dt and const_typ_cd
FUNCTION f_present_val_get (an_r_weeks NUMBER, ad_present_val_dt DATE, an_wcr_rt NUMBER, ad_injr_dt DATE) RETURN NUMBER;
-- This function returns the present value for the weeks and injr_dt passed in.
FUNCTION f_cmpn_law_cnst_hist_val_get (av_cnst_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN NUMBER;
-- This function returns the law constant values for a cnst_typ_cd and LOE date passed in
FUNCTION f_fit_wh_mnm_incm_amt_get (an_earn_val NUMBER, ad_loe_dt DATE, av_fit_wh_file_typ_cd VARCHAR2) RETURN NUMBER;
-- This public function returns the Federal Withholding Minimum Income Amt for the earnest value, file_typ_cd and LOE Date passed in
FUNCTION f_fit_wh_pct_rt_get (an_earn_val NUMBER, ad_loe_dt DATE, av_fit_wh_file_typ_cd VARCHAR2) RETURN NUMBER;
-- This public function returns the Federal Withholding Pct.Rate for the earnest value, file_typ_cd and LOE Date passed in
FUNCTION f_fit_wh_bs_wh_amt_get (an_earn_val NUMBER, ad_loe_dt DATE, av_fit_wh_file_typ_cd VARCHAR2) RETURN NUMBER;
-- This public function returns the Federal Withholding Base Amt. for the earnest value, file_typ_cd and LOE Date passed in
FUNCTION f_aggv_hist_pct_get (an_injr_id NUMBER,ad_admis_eff_dt DATE, ad_admis_end_dt DATE, av_exp_cd VARCHAR2) RETURN NUMBER;
-- This public function returns the Aggravation value for the injr_id, exp_cd and LOE Date passed in
FUNCTION f_mar_sts_get (an_injr_id NUMBER, ad_loe_dt DATE) RETURN VARCHAR2;
-- This public function returns the Marital Status of the injury effective as of the data passed in
FUNCTION f_no_of_deps_get (an_injr_id NUMBER, ad_loe_dt DATE) RETURN NUMBER;
-- This public function returns the # of deps. of the injury effective as of the data passed in
FUNCTION f_ttl_admis_days_get (an_injr_id NUMBER, an_admis_prd NUMBER, av_bnft_typ_cd VARCHAR2) RETURN NUMBER;
-- This public function returns the total admission days for the injury effective as of the data passed in and the benefit type cd
FUNCTION f_admis_prd_cnsc_chk (an_injr_id NUMBER, ad_admis_prd_eff_dt DATE, ad_admis_prd_end_dt DATE, ad_loe_dt DATE, av_btch_ind VARCHAR2, av_bnft_typ_cd VARCHAR2) RETURN VARCHAR2;
-- This public function will return 't' if admission period is consective for 05/10/ of the preceding year to
-- 05/10 of the current year else 'f' for the injr_id, LOE date and passed-in benefit type.*
-- 05/10 is stored in cmpn_law_cnst_hist tables as days & months
FUNCTION f_const_typ_his_val_get (av_const_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN NUMBER;
-- This public function will return the constant type number value for the const_typ_cd and LOE date passed-in
FUNCTION f_const_typ_his_dt_get (av_const_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN DATE;
-- This public function will return the constant type date value for the const_typ_cd and LOE date passed-in
FUNCTION f_const_typ_his_str_get (av_const_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN VARCHAR2;
-- This public function will return the constant type varchar value for the const_typ_cd and LOE date passed-in
FUNCTION f_cur_dth_dt_get (an_injr_id NUMBER) RETURN DATE;
-- This public function will return the death date for the passed in injr_id
FUNCTION f_clm_dth_chk (an_injr_id NUMBER) RETURN VARCHAR2;
-- This public function will return true if death happened after more than a year else false
FUNCTION f_days_not_paid_get (an_injr_id NUMBER, an_wait_prd NUMBER, av_bnft_typ_cd VARCHAR2) RETURN NUMBER;
-- This function will return the days not paid for an injr_id and benefit type--
PROCEDURE prc_adm_recalc_ind_set(an_injr_id NUMBER, an_admis_no NUMBER, av_recalc VARCHAR2);
-- This function will set the recalc indicator to 'y'/'n'
PROCEDURE prc_sch_recalc_ind_set(an_sch_id NUMBER, av_recalc VARCHAR);
-- This function will set the re-schedule indicator to 'y'/'n'
FUNCTION f_ttl_admis_days_get (an_injr_id NUMBER, an_admis_prd NUMBER) RETURN NUMBER;
-- This function returns the total number of admission days for the injr_id passed in
FUNCTION f_days_not_paid_get (an_injr_id NUMBER, an_wait_prd NUMBER, an_admis_no NUMBER, an_admis_prd_no NUMBER) RETURN NUMBER;
-- This function returns the total number of days NOT paid for the injr_id passed in
--

--
END;
/
CREATE OR REPLACE PACKAGE BODY PC_DBA.pkg_clm AS
--
/*****************************************************************************************
 *Package Title     : pkg_clm
 *Description       : This package handles all of the functions for the benfit_calc
                      controller, fetch the data needed for the given block.             *
 *Created By        : Senthil R Avinash                                                  *
 *Creation Date     : 11/05/01                                                           *
 *Run Statements    :                                                                    *
 *****************************************************************************************
 *Maintenance History                                                                    *
 *Date        Author         Description                                               *
 *---------------------------------------------------------------------------------------*
 *11/05/01    Senthil        Generic Claims Controller used by bnft_calc controller
 *                           to fetch data for blocks defined in rating_block_expression *
 *02/17/2017  czheng         tdx14782 fix f_days_not_paid_get (the last function) to look at max rating history record*
 *7/17/2018   czheng         [KIWI-658] change f_present_val_get to determin present worth based on wcr rate
 *2/25/2019   dprice    [KIWI950] Change way lump sum discount is calculated (base on system date instead
 *                                                     of date of injury)
 *4/12/2021   jamartin        comment for testing database scripts.
 ****************************************************************************************/
--
/****************************************************************************************/
/*                    V A R I A B L E S ,   C O N S T A N T S   H E R E                 */
/****************************************************************************************/
--
    GENERAL_ERROR                   EXCEPTION;
--
/****************************************************************************************/
/*       P R I V A T E   F U N C T I O N S   A N D   P R O C E D U R E S   H E R E      */
/****************************************************************************************/
/****************************************************************************************/
/*       P U B L I C   F U N C T I O N S   A N D   P R O C E D U R E S   H E R E      */
/****************************************************************************************/
--
FUNCTION f_lob_cd_get(an_injr_id NUMBER) RETURN VARCHAR2 IS
/*****************************************************************************************
 * This public function will get line of business for the injr_id passed in              *
 ****************************************************************************************/
    lv_lob_cd      VARCHAR2(15);
    lv_err_lbl     VARCHAR2(2000);
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_lob_cd_get>get f_lob_cd_get';
      SELECT
           cch.lob_cd
      INTO
           lv_lob_cd
      FROM
           claim_coverage_history cch,
           injury i
      WHERE
           cch.injr_id = i.injr_id
      AND  cch.lgl_enty_id_emplr = i.lgl_enty_id_emplr
      AND  cch.clm_cov_hist_end_dt IS NULL
      AND  cch.injr_id = an_injr_id;

      IF lv_lob_cd IS NULL THEN
         SELECT
            cth.const_typ_hist_str
         INTO
            lv_lob_cd
         FROM
            constant_type_history cth
         WHERE
            cth.const_typ_cd = 'lob_cd';
      END IF;

      RETURN lv_lob_cd;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        SELECT
           cth.const_typ_hist_str
        INTO
           lv_lob_cd
        FROM
           constant_type_history cth
        WHERE
            cth.const_typ_cd = 'lob_cd';
        RETURN lv_lob_cd;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);

END f_lob_cd_get;
--
/************************************************************************************************/
FUNCTION f_bnft_stt_cd_get(an_injr_id NUMBER) RETURN VARCHAR2 IS
/*****************************************************************************************
 * This public function returns the benefit stt_cd for an injr_id passed in. *
 ****************************************************************************************/
    lv_bnft_stt_cd      VARCHAR2(15);
    lv_err_lbl          VARCHAR2(2000);
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_bnft_stt_cd_get>get f_bnft_stt_cd_get';
      SELECT
           i.bnft_stt_cd
      INTO
           lv_bnft_stt_cd
      FROM
           injury i
      WHERE
           i.injr_id = an_injr_id;

      RETURN lv_bnft_stt_cd;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);

END f_bnft_stt_cd_get;
--
/************************************************************************************************/
FUNCTION f_mrkt_typ_cd_get(an_injr_id NUMBER) RETURN VARCHAR2 IS
/*****************************************************************************************
 * This public function returns the market_type cd for an injr_id passed in.                *
 ****************************************************************************************/
    lv_mrkt_typ_cd      VARCHAR2(15);
    lv_err_lbl			VARCHAR2(2000);
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_mrkt_typ_cd_get>get f_mrkt_typ_cd_get';
      SELECT
           cch.mrkt_typ_cd
      INTO
           lv_mrkt_typ_cd
      FROM
           claim_coverage_history cch,
           injury i
      WHERE
           cch.injr_id = i.injr_id
      AND  cch.lgl_enty_id_emplr = i.lgl_enty_id_emplr
      AND  cch.clm_cov_hist_end_dt IS NULL
      AND  cch.injr_id = an_injr_id;

      IF lv_mrkt_typ_cd IS NULL THEN
         SELECT
            cth.const_typ_hist_str
         INTO
            lv_mrkt_typ_cd
         FROM
            constant_type_history cth
         WHERE
            cth.const_typ_cd = 'mrkt_typ_cd';
      END IF;

      RETURN lv_mrkt_typ_cd;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        SELECT
            cth.const_typ_hist_str
        INTO
            lv_mrkt_typ_cd
        FROM
            constant_type_history cth
        WHERE
            cth.const_typ_cd = 'mrkt_typ_cd';
        RETURN lv_mrkt_typ_cd;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);

END f_mrkt_typ_cd_get;
--
/************************************************************************************************/
FUNCTION f_cmpy_cd_get(an_injr_id NUMBER) RETURN VARCHAR2 IS
/*****************************************************************************************
 * This public function returns the company cd for an injr_id passed in.                  *
 ****************************************************************************************/
    lv_cmpy_cd      VARCHAR2(15);
    lv_err_lbl			VARCHAR2(2000);
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_cmpy_cd_get>get f_cmpy_cd_get';
      SELECT
           cch.cmpy_cd
      INTO
           lv_cmpy_cd
      FROM
           claim_coverage_history cch,
           injury i
      WHERE
           cch.injr_id = i.injr_id
      AND  cch.lgl_enty_id_emplr = i.lgl_enty_id_emplr
      AND  cch.clm_cov_hist_end_dt IS NULL
      AND  cch.injr_id = an_injr_id;

      IF lv_cmpy_cd IS NULL THEN
         SELECT
            cth.const_typ_hist_str
         INTO
            lv_cmpy_cd
         FROM
            constant_type_history cth
         WHERE
            cth.const_typ_cd = 'cmpy_cd';
      END IF;

      RETURN lv_cmpy_cd;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        SELECT
           cth.const_typ_hist_str
        INTO
           lv_cmpy_cd
        FROM
           constant_type_history cth
        WHERE
           cth.const_typ_cd = 'cmpy_cd';
        RETURN lv_cmpy_cd;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);

END f_cmpy_cd_get;
--
/************************************************************************************************/
FUNCTION f_injr_dt_get (an_injr_id NUMBER) RETURN DATE IS
/*****************************************************************************************
 * This public function returns the injury_date for an injr_id passed in.                 *
 ****************************************************************************************/
    ld_injr_dtm     DATE;
    lv_err_lbl			VARCHAR2(2000);
BEGIN
     lv_err_lbl := 'pkg_clm.f_injr_dt_get>get injr_dtm';
     SELECT
          TRUNC(injr_dtm)
     INTO
          ld_injr_dtm
     FROM
          injury
     WHERE
          injr_id = an_injr_id;

     RETURN ld_injr_dtm;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);

END f_injr_dt_get;
--
/************************************************************************************************/
FUNCTION f_loe_get (an_injr_id NUMBER) RETURN DATE IS
/*****************************************************************************************
 * This public function returns the loss of earnings date for an injr_id passed in.       *
 ****************************************************************************************/
    ld_loe           DATE;
    lv_err_lbl			 VARCHAR2(2000);
    lv_stripped_msg  VARCHAR2(3000);
BEGIN
--
     lv_err_lbl := 'pkg_clm.f_loe_get>get loe_eff_dt';
     SELECT
        	clm_loe_eff_dt
     INTO
          ld_loe
     FROM
    	    claim_loe
     WHERE
         	clm_loe_void_ind = 'n'
     AND  injr_id = an_injr_id
     AND  clm_loe_eff_dt = (
          SELECT
              MAX(cl1.clm_loe_eff_dt)
          FROM
              claim_loe cl1
          WHERE
              cl1.injr_id = an_injr_id
          AND cl1.clm_loe_void_ind = 'n');

     RETURN ld_loe;
--
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    lv_err_lbl := pkg_const.excpt_id||'No LOE date exists.'||pkg_const.excpt_id;
    lv_stripped_msg := pkg_excpt_util.f_msg_strip(lv_err_lbl);

    RAISE_APPLICATION_ERROR(pkg_const.unk_err,lv_stripped_msg);
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);

END f_loe_get;
--
/************************************************************************************************/
FUNCTION f_aww_get (an_injr_id NUMBER) RETURN NUMBER IS
--
/*****************************************************************************************
 * This public function returns the current Average Weekly Wage for an injr_id passed in.         *
 ****************************************************************************************/
--
    ln_aww           NUMBER;
    lv_err_lbl			 VARCHAR2(2000);
    lv_stripped_msg  VARCHAR2(3000);
--
BEGIN
--
       lv_err_lbl := 'pkg_clm.f_aww_get>get AWW';
       SELECT
         	 cawwh.clm_aww_hist_aww_amt
       INTO
           ln_aww
       FROM
      	   claim_aww_history cawwh,
           claim_aww caww,
           claim_employment ce
       WHERE
           cawwh.clm_aww_hist_no = (
       		SELECT
           		MAX(cawwh2.clm_aww_hist_no)
            FROM
          	    claim_aww_history cawwh2
            WHERE
          	    cawwh2.clm_aww_hist_void_ind <> 'y'
            AND cawwh2.clm_empl_no = cawwh.clm_empl_no
            AND cawwh2.injr_id = cawwh.injr_id)
          	AND cawwh.clm_empl_no = caww.clm_empl_no
          	AND cawwh.injr_id = caww.injr_id
          	AND caww.clm_empl_no = ce.clm_empl_no
          	AND caww.injr_id = ce.injr_id
          	AND ce.clm_loe_no = (
                SELECT
                    MAX(ce2.clm_loe_no)
                FROM
                    claim_employment ce2
              	WHERE
                    ce2.injr_id = an_injr_id)
       AND ce.injr_id = an_injr_id;

--
    RETURN ln_aww;
--
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    lv_err_lbl := pkg_const.excpt_id||'No AWW exists.'||pkg_const.excpt_id;
    lv_stripped_msg := pkg_excpt_util.f_msg_strip(lv_err_lbl);

    RAISE_APPLICATION_ERROR(pkg_const.unk_err,lv_stripped_msg);
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_aww_get;
--
/************************************************************************************************/
FUNCTION f_post_aww_get (an_injr_id NUMBER, ad_admis_eff_dt DATE, ad_admis_end_dt DATE) RETURN NUMBER IS
/*****************************************************************************************
 * This function returns the current Post AWW for the injr_id and admission period dates passed in.*
 ****************************************************************************************/
--
    ln_post_aww     NUMBER := 0;
    lv_err_lbl		VARCHAR2(2000);
--
BEGIN
      lv_err_lbl := 'pkg_clm.f_post_aww_get>get Post AWW';
      SELECT
      	    SUM(jphd.job_pay_hist_dtl_drv_aww_amt)
      INTO
            ln_post_aww
      FROM
      	    claim_loe cl,
            claim_employment ce,
            job_pay_history_detail jphd,
            job_pay_history jph,
            job j
      WHERE
          	cl.clm_loe_void_ind = 'n'
    	AND cl.injr_id = j.injr_id
    	AND cl.clm_loe_no = ce.clm_loe_no
    	AND ce.clm_empl_typ_cd = 'postinjr'
    	AND ce.clm_empl_no = j.clm_empl_no
     	AND ce.injr_id = j.injr_id
    	AND trunc(jphd.job_pay_hist_dtl_eff_dt) <= ad_admis_eff_dt
    	AND NVL(trunc(jphd.job_pay_hist_dtl_end_dt), ad_admis_end_dt + 1) >= ad_admis_end_dt
    	AND jphd.job_pay_hist_dtl_excl_ind <> 'y'
    	AND jphd.job_pay_hist_no = jph.job_pay_hist_no
    	AND jphd.job_pay_no = jph.job_pay_no
    	AND jphd.job_id = jph.job_id
    	AND jph.job_pay_hist_void_ind <> 'y'
    	AND jph.job_id = j.job_id
    	AND j.injr_id = an_injr_id;
--
      IF ln_post_aww IS NULL THEN
         ln_post_aww := 0;
      END IF;

      RETURN ln_post_aww;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN ln_post_aww;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_post_aww_get;
--
/************************************************************************************************/
FUNCTION f_cmpn_law_cnst_hist_val_get (av_cnst_typ_cd VARCHAR2, ad_injr_dt DATE, av_bnft_typ_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the Law Constant History Value for the const_typ_cd, injr_date *
* and bnft_typ_cd passed in.
* Note: law_bnft_cnst_typ_cd = av_bnft_typ_cd + '_ + av_cnst_typ_cd
 ****************************************************************************************/
--
    ln_cnst_hist_val          NUMBER := 0;
    lv_err_lbl			      VARCHAR2(2000);
--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_cmpn_law_cnst_hist_val_get>get cnst hist val';
      SELECT
            cmlh.cmpn_law_cnst_hist_val
      INTO
            ln_cnst_hist_val
      FROM
            compensation_law_constant_hist cmlh
      WHERE
            cmlh.law_bnft_cnst_typ_cd = av_bnft_typ_cd || '_' || av_cnst_typ_cd
      AND   ad_injr_dt BETWEEN cmlh.cmpn_law_cnst_hist_eff_dt
      AND   NVL(cmlh.cmpn_law_cnst_hist_end_dt, ad_injr_dt + 1)
      AND   cmlh.cmpn_law_cnst_hist_void_ind = 'n';

--
    RETURN ln_cnst_hist_val;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN ln_cnst_hist_val;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_cmpn_law_cnst_hist_val_get;
--
/************************************************************************************************/
FUNCTION f_present_val_get (an_r_weeks NUMBER, ad_present_val_dt DATE, an_wcr_rt NUMBER, ad_injr_dt DATE) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the present value for the weeks and injr_dt passed in.    *
 ****************************************************************************************/
--
-- KHD 522 calculate discount based on system date now instead of DOI
--
--
    ln_val_1_max_rate       NUMBER;
    ln_present_val          NUMBER;
    lv_err_lbl			        VARCHAR2(2000);
--
BEGIN
--    [KIWI-658] - changes made for present value get (determined by wcr rate and injury date)
      lv_err_lbl := 'pkg_clm.f_present_val_get>get present val';
      --get the max rate for present val 1
      BEGIN
        SELECT cth.const_typ_hist_val
        INTO ln_val_1_max_rate
        FROM constant_type_history cth
        WHERE cth.const_typ_cd = 'prsntvl_1_rtmax'
      --KIWI950
      -- AND NVL(ad_injr_dt, ad_present_val_dt)
         AND NVL(ad_present_val_dt, ad_injr_dt)
          BETWEEN cth.const_typ_hist_eff_dt
          --KIWI950
          --AND NVL(cth.const_typ_hist_end_dt, NVL(ad_injr_dt, ad_present_val_dt) + 1);
         AND NVL(cth.const_typ_hist_end_dt, NVL(ad_present_val_dt, ad_injr_dt) + 1);
      EXCEPTION
        WHEN no_data_found THEN
          ln_val_1_max_rate := NULL;
      END;

      IF NOT ln_val_1_max_rate IS NULL AND ln_val_1_max_rate >= an_wcr_rt THEN
        --the wcr rate is within the val 1 max, use val 1 from present worth
        SELECT pwt.prsnt_wrth_typ_val_1
        INTO ln_present_val
        FROM present_worth_type pwt
        WHERE pwt.prsnt_wrth_typ_wek = ROUND(an_r_weeks)
        AND ad_present_val_dt BETWEEN pwt.prsnt_wrth_typ_eff_dt AND NVL(pwt.prsnt_wrth_typ_end_dt, ad_present_val_dt + 1)
        AND pwt.prsnt_wrth_typ_void_ind = 'n';
      ELSE
        --use the default present worth val
        SELECT pwt.prsnt_wrth_typ_val
        INTO ln_present_val
        FROM present_worth_type pwt
        WHERE pwt.prsnt_wrth_typ_wek = ROUND(an_r_weeks)
        AND   ad_present_val_dt BETWEEN pwt.prsnt_wrth_typ_eff_dt AND NVL(pwt.prsnt_wrth_typ_end_dt, ad_present_val_dt + 1)
        AND   pwt.prsnt_wrth_typ_void_ind = 'n';
      END IF;
--
      RETURN ln_present_val;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_present_val_get;
/************************************************************************************************/
FUNCTION f_cmpn_law_cnst_hist_val_get (av_cnst_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the cmpn_law_cnst_hist value for the cnst_typ_cd and LOE date
 ****************************************************************************************/
--
    ln_cnst_hist_val        NUMBER;
    lv_err_lbl			    VARCHAR2(2000);
--
BEGIN
---
      lv_err_lbl := 'pkg_clm.f_cmpn_law_cnst_hist_val_get>get cnst hist val';
      SELECT
            cmlh.cmpn_law_cnst_hist_val
      INTO
            ln_cnst_hist_val
      FROM
            compensation_law_constant_hist cmlh
      WHERE
            cmlh.law_bnft_cnst_typ_cd = av_cnst_typ_cd
      AND   ad_loe_dt BETWEEN cmlh.cmpn_law_cnst_hist_eff_dt
      AND   NVL(cmlh.cmpn_law_cnst_hist_end_dt, ad_loe_dt + 1)
      AND   cmlh.cmpn_law_cnst_hist_void_ind = 'n';

--
    RETURN ln_cnst_hist_val;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_cmpn_law_cnst_hist_val_get;
/************************************************************************************************/
FUNCTION f_fit_wh_mnm_incm_amt_get (an_earn_val NUMBER, ad_loe_dt DATE, av_fit_wh_file_typ_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the Federal Withholding Minimum Income Amt for the earnest value, file_typ_cd
* and LOE Date passed in
 ****************************************************************************************/
--
    ln_wh_mnm_incm_amt        NUMBER;
    lv_err_lbl			    VARCHAR2(2000);
--
BEGIN
---
      lv_err_lbl := 'pkg_clm.f_fit_wh_mnm_incm_amt_get>get mnm_incm_amt';
      SELECT
            fitw.fit_wh_mnm_incm_amt
      INTO
            ln_wh_mnm_incm_amt
      FROM
            federal_income_tax_withholding fitw
      WHERE
            fitw.fit_wh_file_typ_cd = av_fit_wh_file_typ_cd
      AND   an_earn_val >= fitw.fit_wh_mnm_incm_amt
      AND   an_earn_val < fitw.fit_wh_max_incm_amt
      AND   ad_loe_dt BETWEEN fitw.fit_wh_eff_dt AND NVL(fitw.fit_wh_end_dt, ad_loe_dt + 1);
--
    RETURN ln_wh_mnm_incm_amt;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_fit_wh_mnm_incm_amt_get;
/************************************************************************************************/
FUNCTION f_fit_wh_pct_rt_get (an_earn_val NUMBER, ad_loe_dt DATE, av_fit_wh_file_typ_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the Federal Withholding Pct.Rate for the earnest value, file_typ_cd
* and LOE Date passed in
 ****************************************************************************************/
--
    ln_wh_pct_rt            NUMBER;
    lv_err_lbl			    VARCHAR2(2000);
--
BEGIN
---
      lv_err_lbl := 'pkg_clm.f_fit_wh_pct_rt_get>get wh_pct_rt';
      SELECT
            fitw.fit_wh_wh_pct_rt
      INTO
            ln_wh_pct_rt
      FROM
            federal_income_tax_withholding fitw
      WHERE
            fitw.fit_wh_file_typ_cd = av_fit_wh_file_typ_cd
      AND   an_earn_val >= fitw.fit_wh_mnm_incm_amt
      AND   an_earn_val < fitw.fit_wh_max_incm_amt
      AND   ad_loe_dt BETWEEN fitw.fit_wh_eff_dt
      AND   NVL(fitw.fit_wh_end_dt, ad_loe_dt + 1);

--
    RETURN ln_wh_pct_rt;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_fit_wh_pct_rt_get;
--
/************************************************************************************************/
FUNCTION f_fit_wh_bs_wh_amt_get (an_earn_val NUMBER, ad_loe_dt DATE, av_fit_wh_file_typ_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the Federal Withholding Base Amt. for the earnest value, file_typ_cd
* and LOE Date passed in
 ****************************************************************************************/
--
    ln_wh_bs_amt            NUMBER;
    lv_err_lbl			    VARCHAR2(2000);
--
BEGIN
---
      lv_err_lbl := 'pkg_clm.f_fit_wh_bs_wh_amt_get>get bs_wh_amt';
      SELECT
            fitw.fit_wh_bs_wh_amt
      INTO
            ln_wh_bs_amt
      FROM
            federal_income_tax_withholding fitw
      WHERE
            fitw.fit_wh_file_typ_cd = av_fit_wh_file_typ_cd
      AND   an_earn_val >= fitw.fit_wh_mnm_incm_amt
      AND   an_earn_val < fitw.fit_wh_max_incm_amt
      AND   ad_loe_dt BETWEEN fitw.fit_wh_eff_dt
      AND   NVL(fitw.fit_wh_end_dt, ad_loe_dt + 1);

--
    RETURN ln_wh_bs_amt;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_fit_wh_bs_wh_amt_get;
--
/************************************************************************************************/
FUNCTION f_aggv_hist_pct_get (an_injr_id NUMBER, ad_admis_eff_dt DATE, ad_admis_end_dt DATE, av_exp_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the Aggravation value pct for the injr_id, exp_cd and LOE Date
* passed in
 ****************************************************************************************/
--
    ln_aggv_val             NUMBER;
    lv_err_lbl			    VARCHAR2(2000);
--
BEGIN
---
      lv_err_lbl := 'pkg_clm.f_aggv_hist_pct_get>get aggv_hist_pct';
      SELECT
            cah.clm_aggv_hist_pct
      INTO
            ln_aggv_val
      FROM
            claim_aggravation_history cah
      WHERE
            cah.injr_id = an_injr_id
      AND ad_admis_eff_dt BETWEEN cah.clm_aggv_hist_eff_dt  AND NVL(cah.clm_aggv_hist_end_dt,NVL(ad_admis_end_dt,ad_admis_eff_dt))
      AND NVL(ad_admis_end_dt,ad_admis_eff_dt) BETWEEN cah.clm_aggv_hist_eff_dt  AND NVL(cah.clm_aggv_hist_end_dt,NVL(ad_admis_end_dt,ad_admis_eff_dt))
      AND   cah.exp_cd = av_exp_cd
      AND   cah.clm_aggv_hist_void_ind = 'n';
--
    RETURN ln_aggv_val/100;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 1;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_aggv_hist_pct_get;
--
FUNCTION f_mar_sts_get (an_injr_id NUMBER, ad_loe_dt DATE) RETURN VARCHAR2 IS
--
/*****************************************************************************************
* This public function returns the Marital Status of the LOE dt as of the data passed in *
 ****************************************************************************************/
--
    lv_mar_sts_cd           VARCHAR2(15);
    lv_err_lbl			    VARCHAR2(2000);
    ln_hold_lst_cnt         NUMBER := 0;
    lv_hold_lst             PKG_STD.Vchar15Lst;
    ld_hold_lst             PKG_STD.DATELST;
--
BEGIN
---
      lv_err_lbl := 'pkg_clm.f_mar_sts_get>get mar_sts_cd';
      BEGIN
          SELECT
              mar_sts_cd
          INTO
              lv_mar_sts_cd
          FROM
              person_history ph,
              injury i
          WHERE
              ph.lgl_enty_id_prsn = i.lgl_enty_id_clmt
              AND i.injr_id = an_injr_id
              AND ph.prsn_hist_void_ind = 'n'
              AND ph.prsn_hist_no =
                  ( SELECT
                        MAX(sph.prsn_hist_no)
                    FROM
                        person_history sph,
                        injury si
                    WHERE
                        sph.lgl_enty_id_prsn = si.lgl_enty_id_clmt
                        AND si.injr_id = an_injr_id
                        AND sph.prsn_hist_void_ind = 'n' );
      END;
--
      RETURN lv_mar_sts_cd;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 's'; -- return single if none found
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_mar_sts_get;
--
/************************************************************************************************/
FUNCTION f_no_of_deps_get (an_injr_id NUMBER, ad_loe_dt DATE) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function returns the # of deps. of the LOE dt as of the data passed in *
-- Used by Batch Routines for Admissions scheduling and calculations
 ****************************************************************************************/
--
    ln_no_deps             NUMBER;
    lv_err_lbl			       VARCHAR2(2000);
--
BEGIN
---
    lv_err_lbl := 'pkg_clm.f_no_of_deps_get>get no_deps';
    SELECT
          COUNT(*)
    INTO
          ln_no_deps
    FROM
      	dependent d,
      	injury i
    WHERE
      	i.injr_id = an_injr_id
    AND d.lgl_enty_id_prsn = i.lgl_enty_id_clmt
    AND ad_loe_dt BETWEEN d.dep_dep_eff_dt
    AND NVL(d.dep_dep_end_dt, pkg_std.c_high_dt);
--
    RETURN ln_no_deps;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_no_of_deps_get;
--
/************************************************************************************************/
FUNCTION f_ttl_admis_days_get (an_injr_id NUMBER, an_admis_prd NUMBER, av_bnft_typ_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function will return the total number of days for which admissions have been
* entered for the passed-in benefit type.*
 ****************************************************************************************/
--
    ln_ttl_days                   NUMBER;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--

      lv_err_lbl := 'pkg_clm.f_ttl_admis_days_get>get ttl_admis_days_get';
      SELECT
          SUM (( ap.admis_prd_end_dt - ap.admis_prd_eff_dt ) + 1)
      INTO
          ln_ttl_days
      FROM
		  admission a,
          admission_period ap
      WHERE
		  ap.injr_id = a.injr_id
      AND ap.admis_no = a.admis_no
      AND a.bnft_typ_cd = av_bnft_typ_cd
      AND a.admis_typ_cd = 'indm'
      AND a.admis_void_ind = 'n'
      AND ap.admis_prd_void_ind = 'n'
      AND ap.injr_id = an_injr_id;
--
    RETURN NVL(ln_ttl_days, 0) + an_admis_prd;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN an_admis_prd;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_ttl_admis_days_get;
--
/************************************************************************************************/
FUNCTION f_admis_prd_cnsc_chk (an_injr_id NUMBER, ad_admis_prd_eff_dt DATE, ad_admis_prd_end_dt DATE, ad_loe_dt DATE, av_btch_ind VARCHAR2, av_bnft_typ_cd VARCHAR2) RETURN VARCHAR2 IS
--
/***********************************************************************************************
* This public function will return 't' if admission period is consective for 05/10/ of the preceding year to
* 05/10 of the current year else 'f' for the injr_id, LOE date and passed-in benefit type.*
* 05/10 is stored in cmpn_law_cnst_hist tables as days & months
************************************************************************************************/
--
    ld_cola_eff                   DATE;
    ld_adm_eff_dt                 DATE;
    ln_cola_dm                    NUMBER;
    ln_cola_day                   NUMBER;
    ln_cola_mth                   NUMBER;
    ln_cola_year                  NUMBER;
    ln_ttl_days                   NUMBER;
    ln_indx                       NUMBER := 0;
    lv_err_lbl			          VARCHAR2(2000);
    lv_ret_val                    VARCHAR2(15) := PKG_CONST.false;
    lv_cnsv                       VARCHAR2(15) := PKG_CONST.true;
    TYPE admis_prd_rec_typ IS RECORD(
        admis_prd_eff_dt            DATE,
        admis_prd_end_dt            DATE);
    --
    TYPE admis_prd_rec_lst IS TABLE OF admis_prd_rec_typ
    	INDEX BY BINARY_INTEGER;
    --
    lrec_admis_prd_lst                 admis_prd_rec_lst;
    --

    CURSOR admis_prd_cur_new IS
      SELECT
          ap.admis_prd_eff_dt a,
          ap.admis_prd_end_dt b
      FROM
		  admission a,
          admission_period ap
      WHERE
		  ap.injr_id = a.injr_id
      AND ap.admis_no = a.admis_no
      AND a.bnft_typ_cd = av_bnft_typ_cd
      AND a.admis_typ_cd = 'indm'
      AND a.admis_void_ind = 'n'
      AND ap.admis_prd_void_ind = 'n'
      AND ap.injr_id = an_injr_id
      UNION
      SELECT
          ad_admis_prd_eff_dt a,
          ad_admis_prd_end_dt b
      FROM
          DUAL
      ORDER BY
          a, b;
    CURSOR admis_prd_cur IS
      SELECT
          ap.admis_prd_eff_dt,
          ap.admis_prd_end_dt
      FROM
		  admission a,
          admission_period ap
      WHERE
		  ap.injr_id = a.injr_id
      AND ap.admis_no = a.admis_no
      AND a.bnft_typ_cd = av_bnft_typ_cd
      AND a.admis_typ_cd = 'indm'
      AND a.admis_void_ind = 'n'
      AND ap.admis_prd_void_ind = 'n'
      AND ap.injr_id = an_injr_id
      ORDER BY
          ap.admis_prd_eff_dt,
          ap.admis_prd_end_dt;


--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_admis_prd_cnsc_chk>get cola day & mth';
      ln_cola_dm  := f_cmpn_law_cnst_hist_val_get ('cola_end_dm', ad_loe_dt, av_bnft_typ_cd);

      IF length(to_char(ln_cola_dm)) = 3 THEN
         ln_cola_mth := SUBSTR(to_char(ln_cola_dm), 1, 1);
         ln_cola_day := SUBSTR(to_char(ln_cola_dm), 2, 2);
      ELSIF length(to_char(ln_cola_dm)) = 4 THEN
         ln_cola_mth := SUBSTR(to_char(ln_cola_dm), 1, 2);
         ln_cola_day := SUBSTR(to_char(ln_cola_dm), 3, 2);
      END IF;
      --
      IF av_btch_ind = 'n' THEN
        lv_err_lbl := 'pkg_clm.f_admis_prd_cnsc_chk>load admis_prd dates into temp tbl';
        FOR admis_prd_rec IN admis_prd_cur_new LOOP
          ln_indx := ln_indx + 1;
          lrec_admis_prd_lst(ln_indx).admis_prd_eff_dt := admis_prd_rec.a;
          lrec_admis_prd_lst(ln_indx).admis_prd_end_dt := admis_prd_rec.b + 1;
        END LOOP;
      ELSE
        lv_err_lbl := 'pkg_clm.f_admis_prd_cnsc_chk>load admis_prd dates into temp tbl';
        FOR admis_prd_rec IN admis_prd_cur LOOP
          ln_indx := ln_indx + 1;
          lrec_admis_prd_lst(ln_indx).admis_prd_eff_dt := admis_prd_rec.admis_prd_eff_dt;
          lrec_admis_prd_lst(ln_indx).admis_prd_end_dt := admis_prd_rec.admis_prd_end_dt + 1;
        END LOOP;
      END IF;
      --
      lv_err_lbl := 'pkg_clm.f_admis_prd_cnsc_chk>chk cnsv dates';
      IF ln_indx > 0 THEN
        FOR y IN 1..ln_indx LOOP
          EXIT WHEN (y + 1) > ln_indx;
          IF lrec_admis_prd_lst(y).admis_prd_end_dt = lrec_admis_prd_lst(y + 1).admis_prd_eff_dt THEN
             lv_cnsv := pkg_const.true;
          ELSE
             lv_cnsv := pkg_const.false;
             ld_adm_eff_dt := lrec_admis_prd_lst(y + 1).admis_prd_eff_dt;
             EXIT;
          END IF;
        END LOOP;
      END IF;
      --
      lv_err_lbl := 'pkg_clm.f_admis_prd_cnsc_chk>cola chk';
      IF lv_cnsv = pkg_const.true THEN
        ld_adm_eff_dt := lrec_admis_prd_lst(1).admis_prd_eff_dt;
      END IF;

      ln_cola_year := TO_CHAR(ad_admis_prd_eff_dt, 'YYYY');
      ld_cola_eff := to_date(ln_cola_day || '/' || ln_cola_mth || '/' || ln_cola_year, 'dd/mm/yyyy');

      IF ad_admis_prd_end_dt <= ld_cola_eff THEN
         ln_cola_year := ln_cola_year - 1 ;
         ld_cola_eff := to_date(ln_cola_day || '/' || ln_cola_mth || '/' || ln_cola_year , 'dd/mm/yyyy');
      END IF;

      WHILE ld_cola_eff < ad_admis_prd_end_dt LOOP
        ln_ttl_days := ld_cola_eff - ld_adm_eff_dt;
        IF ln_ttl_days >= 364 THEN
           lv_ret_val := PKG_CONST.true;
           EXIT;
        END IF;
        ld_cola_eff := ADD_MONTHS(ld_cola_eff, 12);
      END LOOP;
      --
      RETURN lv_ret_val;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN PKG_CONST.FALSE;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_admis_prd_cnsc_chk;
/************************************************************************************************/
FUNCTION f_const_typ_his_val_get (av_const_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function will return the constant type number value for the const_typ_cd and LOE date
* passed-in *
 ****************************************************************************************/
--
    ln_hist_val                   NUMBER;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_const_typ_his_val_get>get const_typ_hist_val';
      SELECT
          cth.const_typ_hist_val
      INTO
          ln_hist_val
      FROM
		  constant_type_history cth
      WHERE
          cth.const_typ_cd = av_const_typ_cd
      AND ad_loe_dt >= cth.const_typ_hist_eff_dt
      AND ad_loe_dt < NVL(cth.const_typ_hist_end_dt, ad_loe_dt + 1);
--
    RETURN ln_hist_val;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_const_typ_his_val_get;
--
/************************************************************************************************/
FUNCTION f_const_typ_his_dt_get (av_const_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN DATE AS
/*****************************************************************************************
* This public function will return the constant type date value for the const_typ_cd and LOE date
* passed-in *
 ****************************************************************************************/
--
    ld_hist_val                   DATE;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_const_typ_his_val_get>get const_typ_hist_val';
      SELECT
          cth.const_typ_hist_dt
      INTO
          ld_hist_val
      FROM
		  constant_type_history cth
      WHERE
          cth.const_typ_cd = av_const_typ_cd
      AND ad_loe_dt >= cth.const_typ_hist_eff_dt
      AND ad_loe_dt < NVL(cth.const_typ_hist_end_dt, ad_loe_dt + 1);
--
    RETURN ld_hist_val;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_const_typ_his_dt_get;
--
/************************************************************************************************/
FUNCTION f_const_typ_his_str_get (av_const_typ_cd VARCHAR2, ad_loe_dt DATE) RETURN VARCHAR2 AS
/*****************************************************************************************
* This public function will return the constant type date value for the const_typ_cd and LOE date
* passed-in *
 ****************************************************************************************/
--
    lv_hist_val                   VARCHAR2(30);
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_const_typ_his_val_get>get const_typ_hist_val';
      SELECT
          cth.const_typ_hist_str
      INTO
          lv_hist_val
      FROM
		  constant_type_history cth
      WHERE
          cth.const_typ_cd = av_const_typ_cd
      AND ad_loe_dt >= cth.const_typ_hist_eff_dt
      AND ad_loe_dt < NVL(cth.const_typ_hist_end_dt, ad_loe_dt + 1);
--
    RETURN lv_hist_val;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_const_typ_his_str_get;
--
/************************************************************************************************/
FUNCTION f_cur_dth_dt_get (an_injr_id NUMBER) RETURN DATE IS
--
/*****************************************************************************************
* This public function will return the death date for the passed in injr_id
 ****************************************************************************************/
--
    ld_dth_dt                     DATE;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_cur_dth_dt_get>get prsn_hist_dth_dt';
      SELECT
		pvc.prsn_hist_dth_dt
      INTO
        ld_dth_dt
      FROM
		person_view_cur pvc,
		injury i
      WHERE
		i.injr_id = an_injr_id AND
		i.lgl_enty_id_clmt = pvc.lgl_enty_id_prsn;
--
    RETURN ld_dth_dt;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_cur_dth_dt_get;
--
/************************************************************************************************/
FUNCTION f_clm_dth_chk (an_injr_id NUMBER) RETURN VARCHAR2 IS
--
/*****************************************************************************************
* This public function will return true if death happened after more than a year else false
 ****************************************************************************************/
--
    ld_dth_dt           DATE;
    lv_err_lbl			VARCHAR2(2000);
--
BEGIN
--
      lv_err_lbl := 'pkg_clm.f_clm_dth_chk>chk dth dt wrt today';
      ld_dth_dt := pkg_clm.f_cur_dth_dt_get(an_injr_id);
      IF (sysdate - NVL(ld_dth_dt, sysdate)) >= 365 THEN
         RETURN pkg_const.true;
      END IF;

      RETURN pkg_const.false;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_clm_dth_chk;
--
/************************************************************************************************/
FUNCTION f_days_not_paid_get (an_injr_id NUMBER, an_wait_prd NUMBER, av_bnft_typ_cd VARCHAR2) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function will return the total number of days NOT paid for the passed in benefit_type and injr_id
* The number of waiting period days (7 for KEMI, 3 for RIBMIC) is passed in too.
* Note: days not paid cannot be greater than 7. If so, return 7 (for KEMI)
 ****************************************************************************************/
--
    ln_days_not_paid              NUMBER;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--

      lv_err_lbl := 'pkg_clm.f_days_not_paid_get>get dd_not_pd';
      SELECT
               SUM(aprh.admis_prd_rt_hist_dd_not_pd)
      INTO
               ln_days_not_paid
      FROM
               admission a,
               admission_period ap,
               admission_period_rate_history  aprh
      WHERE
               ( ap.injr_id = a.injr_id )
      AND      ( ap.admis_no = a.admis_no )
      AND      ( aprh.injr_id = ap.injr_id )
      AND      ( aprh.admis_no = ap.admis_no )
      AND      ( aprh.admis_prd_no = ap.admis_prd_no )
      AND      ( a.injr_id = an_injr_id )
      AND      ( a.bnft_typ_cd = av_bnft_typ_cd )
      AND      ( a.admis_void_ind = 'n' )
      AND      ( ap.admis_prd_void_ind = 'n' )
      AND      ( aprh.admis_prd_rt_hist_void_ind = 'n' )
      GROUP BY a.bnft_typ_cd;

--
    IF NVL(ln_days_not_paid, 0) >= an_wait_prd THEN
       RETURN an_wait_prd;
    ELSE
       RETURN NVL(ln_days_not_paid, 0);
    END IF;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_days_not_paid_get;
--
/************************************************************************************************/
--
PROCEDURE prc_adm_recalc_ind_set(
            an_injr_id NUMBER, an_admis_no NUMBER, av_recalc VARCHAR2) IS
/************************************************************************************************
 * Update the admission recalc indicator to 'y'.
 *
 ************************************************************************************************/
--
    lv_err_lbl                     VARCHAR2(200);
--
BEGIN
--
    lv_err_lbl := 'admission recalc ind update';
    UPDATE
        admission a
    SET
        a.admis_recalc_ind = av_recalc
    WHERE
        a.injr_id = an_injr_id
    AND a.admis_no = an_admis_no;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END prc_adm_recalc_ind_set;
--
/************************************************************************************************/
--
PROCEDURE prc_sch_recalc_ind_set(an_sch_id NUMBER, av_recalc VARCHAR) IS
/************************************************************************************************
 * Update the admission recalc indicator to 'y'.
 *
 ************************************************************************************************/
--
    lv_err_lbl                     VARCHAR2(200);
--
BEGIN
--
    lv_err_lbl := 'admission schedule recalc ind update';
    UPDATE
        admission_schedule a
    SET
        a.admis_sch_resch_ind = av_recalc
    WHERE
        a.admis_sch_id = an_sch_id;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END prc_sch_recalc_ind_set;
--
/************************************************************************************************/
--
PROCEDURE prc_days_not_pd_updt(
            an_injr_id NUMBER) IS
/************************************************************************************************
 * This procedure will update the Days Not Paid to 0 and set the recalc indicator to 'y'
 *
 ************************************************************************************************/
--
    CURSOR cur_dd_not_pd IS
    SELECT
        aprh.admis_no,
        aprh.admis_prd_no,
        aprh.admis_prd_rt_hist_dd_not_pd
    FROM
        admission a,
        admission_period ap,
        admission_period_rate_history aprh
    WHERE
        a.injr_id = ap.injr_id
    AND ap.injr_id = aprh.injr_id
    AND a.admis_no = ap.admis_no
    AND ap.admis_no = aprh.admis_no
    AND ap.admis_prd_no = aprh.admis_prd_no
    AND a.injr_id = an_injr_id
    AND aprh.admis_prd_rt_hist_no = 1
    AND aprh.admis_prd_rt_hist_void_ind = 'n'
    AND ap.admis_prd_void_ind = 'n'
    AND a.admis_void_ind = 'n'
    AND aprh.admis_prd_rt_hist_dd_not_pd > 0
    ORDER BY ap.admis_no, ap.admis_prd_no;

    lv_err_lbl                     VARCHAR2(200);
--
BEGIN
--
FOR dd_not_pd_rec IN cur_dd_not_pd LOOP

    UPDATE
        admission_period_rate_history aprh
    SET
        aprh.admis_prd_rt_hist_dd_not_pd = 0
    WHERE
        aprh.injr_id = an_injr_id
    AND aprh.admis_no = dd_not_pd_rec.admis_no
    AND aprh.admis_prd_no = dd_not_pd_rec.admis_prd_no
    AND aprh.admis_prd_rt_hist_no = 1;

    pkg_clm.prc_adm_recalc_ind_set(an_injr_id, dd_not_pd_rec.admis_no, 'y');

END LOOP;

--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END prc_days_not_pd_updt;
--
/************************************************************************************************/
FUNCTION f_ttl_admis_days_get (an_injr_id NUMBER, an_admis_prd NUMBER) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function will return the total number of admissions days for the injr_id *
 ****************************************************************************************/
--
    ln_ttl_days                   NUMBER;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--

      lv_err_lbl := 'pkg_clm.f_ttl_admis_days_get>get ttl_admis_days_get';
      SELECT
          SUM (( ap.admis_prd_end_dt - ap.admis_prd_eff_dt ) + 1)
      INTO
          ln_ttl_days
      FROM
		  admission a,
          admission_period ap
      WHERE
		  ap.injr_id = a.injr_id
      AND ap.admis_no = a.admis_no
      AND a.admis_typ_cd = 'indm'
      AND a.admis_void_ind = 'n'
      AND ap.admis_prd_void_ind = 'n'
      AND ap.injr_id = an_injr_id;
--
    RETURN NVL(ln_ttl_days, 0) + an_admis_prd;
--
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN an_admis_prd;
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_ttl_admis_days_get;
--
/************************************************************************************************/
FUNCTION f_days_not_paid_get (an_injr_id NUMBER, an_wait_prd NUMBER, an_admis_no NUMBER, an_admis_prd_no NUMBER) RETURN NUMBER IS
--
/*****************************************************************************************
* This public function will return the total number of days NOT paid for the injr_id passed in *
 ****************************************************************************************/
--
    ln_days_not_paid              NUMBER;
    ln_cur_days_not_pd            NUMBER;
    lv_err_lbl			          VARCHAR2(2000);
--
BEGIN
--

      lv_err_lbl := 'pkg_clm.f_days_not_paid_get>get dd_not_pd';
      BEGIN
          SELECT
                   SUM(aprh.admis_prd_rt_hist_dd_not_pd)
          INTO
                   ln_days_not_paid
          FROM
                   admission a,
                   admission_period ap,
                   admission_period_rate_history  aprh
          WHERE
                   a.injr_id = ap.injr_id
          AND      ap.injr_id = aprh.injr_id
          AND      a.admis_no = ap.admis_no
          AND      ap.admis_no = aprh.admis_no
          AND      ap.admis_prd_no = aprh.admis_prd_no
          AND      aprh.admis_prd_rt_hist_no = (
            SELECT MAX(aprh2.admis_prd_rt_hist_no)
            FROM admission_period_rate_history aprh2
            WHERE aprh2.injr_id = aprh.injr_id
            AND aprh2.admis_no = aprh.admis_no
            AND aprh2.admis_prd_no = aprh.admis_prd_no
          )
          AND      a.injr_id = an_injr_id
          AND      a.admis_void_ind = 'n'
          AND      ap.admis_prd_void_ind = 'n'
          AND      aprh.admis_prd_rt_hist_void_ind = 'n';
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RETURN 0;
      END;

      BEGIN
          SELECT
                   aprh.admis_prd_rt_hist_dd_not_pd
          INTO
                   ln_cur_days_not_pd
          FROM
           		   admission a,
                   admission_period ap,
                   admission_period_rate_history aprh
          WHERE
                   a.injr_id = ap.injr_id
          AND      ap.injr_id = aprh.injr_id
          AND      a.admis_no = ap.admis_no
          AND      ap.admis_no = aprh.admis_no
          AND      ap.admis_prd_no = aprh.admis_prd_no
          AND      aprh.admis_prd_rt_hist_no = (
            SELECT MAX(aprh2.admis_prd_rt_hist_no)
            FROM admission_period_rate_history aprh2
            WHERE aprh2.injr_id = aprh.injr_id
            AND aprh2.admis_no = aprh.admis_no
            AND aprh2.admis_prd_no = aprh.admis_prd_no
          )
          AND      a.injr_id = an_injr_id
          AND      ap.admis_no =  an_admis_no
          AND      ap.admis_prd_no = an_admis_prd_no
          AND      a.admis_void_ind = 'n'
          AND      ap.admis_prd_void_ind = 'n'
          AND      aprh.admis_prd_rt_hist_void_ind = 'n';
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
              IF NVL(ln_days_not_paid, 0) >= an_wait_prd THEN
                 RETURN an_wait_prd;
              ELSE
                 RETURN NVL(ln_days_not_paid, 0);
              END IF;
      END;

      ln_days_not_paid := ln_days_not_paid - ln_cur_days_not_pd;
      IF NVL(ln_days_not_paid, 0) >= an_wait_prd THEN
         RETURN an_wait_prd;
      ELSE
         RETURN NVL(ln_days_not_paid, 0);
      END IF;
--
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(pkg_const.unk_err, lv_err_lbl||' '||SQLERRM);
--
END f_days_not_paid_get;
--
/************************************************************************************************/
END pkg_clm;
/
