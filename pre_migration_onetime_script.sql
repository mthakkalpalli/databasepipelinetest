--COMPILE AND DOCUMENT INVALID OBJECTS
--JAM: This section creates a compilation script for any invalid objects, runs it once, and recreates for any objects that did not compile the first time.
--scriptfile spool log file
@ENVPATH/scripts/cr_compile_script.sql ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
@ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
@ENVPATH/scripts/cr_compile_script.sql ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
--JAM: These settings change SQLPlus environment and format the output of commands.
set feedback on
set heading on
set echo on
set verify on
set termout on
set pagesize 14
--JAM: create logfile for compilation, counts, etc
spool ENVPATH/projects/stage/TASK_ID/log/TASK_ID_recompile_before_&_CONNECT_IDENTIFIER..log
--JAM: run recompile again
@ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
--JAM: get count of invalid objects, disabled contraints, and disabled triggers in main schemas
@ENVPATH/scripts/invalid_obj_select.sql
--JAM: get a count of the deployment users objects for comparison later
@ENVPATH/scripts/pre_db_migration.sql
spool off
--JAM: These settings change SQLPlus environment and format the output of commands.
set pages 3000
set lines 100
set linesize 4000
set serveroutput on size 32000
set feedback on
set heading on
set echo on
--JAM: create a new logfile for deployment script output
spool ENVPATH/projects/stage/TASK_ID/log/TASK_ID_&_CONNECT_IDENTIFIER..log
set define off