spool off
set define '&'
--JAM: This section creates a compilation script for any invalid objects, runs it once, and recreates for any objects that did not compile the first time.
--JAM: this happens again after deployment
@ENVPATH/scripts/cr_compile_script.sql ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
@ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
@ENVPATH/scripts/cr_compile_script.sql ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
set feedback on
set heading on
set echo on
set verify on
set termout on
set pagesize 14
--JAM: create logfile for post-deployment compilation, counts, etc
spool ENVPATH/projects/stage/TASK_ID/log/TASK_ID_recompile_after_&_CONNECT_IDENTIFIER..log
--JAM: run recompile again
@ENVPATH/projects/stage/TASK_ID/TASK_ID_recompile_&_CONNECT_IDENTIFIER..sql
--JAM: get count of invalid objects, disabled contraints, and disabled triggers in main schemas post-deployment
@ENVPATH/scripts/invalid_obj_select.sql
--JAM: get a count of the deployment users objects after the deployment. Any change from the earlier count is a sign of missing schema prefixes in deployment scripts
@ENVPATH/scripts/post_db_migration.sql
spool off
exit